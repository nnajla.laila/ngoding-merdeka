package com.ddp2.tugasakhir;

public class Mahasiswa {
    private String nama;
    private String kelas;
    private String kodeAsdos;

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public void setKelas (String kelas){
        this.kelas = kelas;
    }

    public void setKodeAsdos (String kode){
        this.kodeAsdos = kode;
    }



    public String getKodeAsdos(){
        return this.kodeAsdos;
    } 

    public String getKelas(){
        return this.kelas;
    }
}