package com.ddp2.tugasakhir;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class MainController {
    
    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/asdos-home")
    public String asdosHome(Model model) {
        return "asdos-home";
    }

    @GetMapping("/mahasiswa-home")
    public String mahasiswaHome(Model model) {
        return "mahasiswa-home";
    }

    @GetMapping ("/form-jadwal")
    public String formJadwal (Model model){
        model.addAttribute("asdos", new Asdos());
        return "form-jadwal";
    }

    @PostMapping("/form-jadwal")
    public String submitFormJadwal(@ModelAttribute Asdos asdos, @RequestParam("tanggaldemo") String date, @RequestParam("waktudemo")String time) {
        
        if(Asdos.getDaftarAsdos().size() != 0){
            for (Asdos data : Asdos.getDaftarAsdos()){
                if (data.getNama().equalsIgnoreCase(asdos.getNama())){
                    data.addDate(date);
                    data.addTime(time);
                } else {
                    Asdos.addAsdos(asdos);
                    asdos.addDate(date);
                    asdos.addTime(time);
                }
            }  
        } else {
            Asdos.addAsdos(asdos);
            asdos.addDate(date);
            asdos.addTime(time);
        }
            
        
       
        
        System.out.println(date);
        return "hasil-form-jadwal";
    }

    @GetMapping ("/form-pilih-jadwal")
    public String formPilihJadwal (Model model){
        model.addAttribute("mahasiswa", new Mahasiswa());
        return "form-pilih-jadwal";
    }

    
    @PostMapping("/form-pilih-jadwal")
    public String submitFormPilihJadwal(Model model, @ModelAttribute Mahasiswa mahasiswa){
        /*Asdos asdos_dummy = new Asdos(); 
        asdos_dummy.setNama("a");
        asdos_dummy.setKodeAsdos("b");
        asdos_dummy.setKelas("c");
        asdos_dummy.addDate("d");
        asdos_dummy.addTime("e");*/
       
       for (Asdos asdos: Asdos.getDaftarAsdos()){

            if (mahasiswa.getKodeAsdos().equalsIgnoreCase(asdos.getKodeAsdos())){
                model.addAttribute( "asdos",asdos );
                return "hasil-form-pilih-jadwal";
            } else {
                return "belum-ada-jadwal";
            }
       }    
       return "hasil-form-pilih-jadwal";
    }

   

    @GetMapping ("/list-jadwal")
    public String listJadwal (Model model){
        model.addAttribute("daftarAsdos", Asdos.getDaftarAsdos());
        return "list-jadwal";
    }

}
    
