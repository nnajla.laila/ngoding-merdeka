package com.ddp2.tugasakhir;
import java.util.ArrayList;

public class Asdos {
    private String nama;
    private String kodeAsdos;
    private String kelas;
    private static ArrayList<Asdos> daftarAsdos = new ArrayList<Asdos>();
    private ArrayList<String> daftarDate = new ArrayList<String>();
    private ArrayList<String> daftarTime = new ArrayList<String>();
    public String getNama() {
        return this.nama;
    }

    public String getKodeAsdos(){
        return this.kodeAsdos;
    }

    public String getKelas(){
        return this.kelas;
    }

    public void setNama (String name){
        this.nama = name;
    }

    public void setKodeAsdos(String kode){
        this.kodeAsdos = kode;
    }

    public void setKelas (String kelas){
        this.kelas = kelas;
    }

    public static ArrayList<Asdos> getDaftarAsdos() {
        if (daftarAsdos != null){
            return daftarAsdos;
        }
        return null;
        
    }

    // fungsi untuk menambahkan asdos baru ke dalam daftarAsdos
    public static void addAsdos(Asdos asdos) {
        daftarAsdos.add(asdos);
    }

 
    public void addDate (String some){
        daftarDate.add(some);
    }

    public void addTime (String yes){
        daftarTime.add(yes);        
    }

    public ArrayList<String> getDaftarDate() {
        return daftarDate;
    }

    public ArrayList<String> getDaftarTime() {
        return daftarTime;
    }


}